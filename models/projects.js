"use strict";

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('projects', {
        id: {type:DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        proj_tyyppi: {type:DataTypes.STRING},
        proj_laatu: {type:DataTypes.TEXT},
        proj_nimi: {type:DataTypes.TEXT},
        proj_katuosoite: {type:DataTypes.TEXT},
        proj_huoneisto: {type:DataTypes.TEXT},
        proj_postiKnumero: {type:DataTypes.TEXT},
        proj_kaupunki: {type:DataTypes.TEXT},
        proj_vastuu: {type:DataTypes.TEXT},
        proj_alkaminen: {type:DataTypes.DATE},
        proj_loppuminen: {type:DataTypes.DATE},
        proj_asiakas: {type:DataTypes.TEXT},
        proj_tila: {type:DataTypes.INTEGER},
        proj_yhteyshenk: {type:DataTypes.TEXT},
        proj_kuvaus: {type:DataTypes.TEXT},
        proj_koko: {type:DataTypes.INTEGER},
        proj_yks: {type:DataTypes.TEXT},
        proj_tieto: {type:DataTypes.TEXT},
        proj_ensi: {type:DataTypes.DATE},
        proj_vali: {type:DataTypes.DATE},
        proj_loppu: {type:DataTypes.DATE},
        proj_kuivauslaskutettu: {type:DataTypes.DATE},
        proj_tyse: {type:DataTypes.DATE},
        proj_asukas: {type:DataTypes.TEXT},
        proj_asu_puh: {type:DataTypes.TEXT},
        proj_laskut: {type:DataTypes.TEXT},
        proj_laskuvalmis: {type:DataTypes.INTEGER},
        proj_ohje: {type:DataTypes.TEXT},
        proj_laskutusosoite: {type:DataTypes.TEXT},
        proj_tieto2: {type:DataTypes.TEXT}
  },{freezeTableName: true});
};
