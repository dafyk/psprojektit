"use strict";
var scrypt = require('scrypt'),
    config = require(process.cwd() + '/config.js');

module.exports = function(sequelize, DataTypes) {

    /**
     * Creates hash from password
     * @param {Object} user - sequelize user model
     * @param {Object} options - sequelize options object
     * @callback callback - sequelize callback
     * @returns {Function} - sequelize callback
     */
    var createHash = function(user, options, callback) {
        scrypt.hash(user.get('password'), {N: 2, r:1, p:1},32,config.app.salt, function(err, hash) {
            if (err) return callback(err);
            user.set('password', hash.toString('hex'));
            return callback(null, options);
        });
    };

    /**
     * Checks if password was changed and in case of change creates hash
     * @callback createHash
     * @param user - sequelize user model
     * @param options - sequelize options object
     * @param callback - sequelize callback
     * @returns {Function} - createHash
     */
    var processPassword = function(user, options, callback) {
        if (!user.changed('password')) return callback(null, options);
        if (user.password) {
            return createHash(user, options, callback);
        } else {
            return callback(null, options);
        }
    };

    // "USER" is reserved keyword in MySQL, use plural "users" for table name
    return sequelize.define('users',
        {
            id: {type:DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
            firstName: {type:DataTypes.STRING},
            lastName: {type:DataTypes.STRING},
            email: {type:DataTypes.STRING,allowNull:false,unique:true,validate:{isEmail:true}},
            role: {type:DataTypes.STRING,defaultValue:'user'},
            password: {type:DataTypes.STRING,allowNull:false}
        },
        {
            freezeTableName: true,
            indexes: [{unique:true,fields:['email']}],
            hooks: {
                beforeCreate:processPassword,
                beforeUpdate:processPassword
            }
        });
};
