var epilogue = require('epilogue'),
    express = require('express'),
    bodyParser = require('body-parser'),
    _ = require('lodash'),
    basicauth = require('basicauth-middleware'),
    auth = require('basic-auth'),
    cors = require('cors'),
    models  = require('./models/'),
    config = require('./config'),
    boom = require('express-boom'),
    app = express();

/**
 * Verify user name and password against array of users stored in config
 * @param user {String}
 * @param pass {String}
 * @returns {boolean}
 */
var checkAuth = function(user,pass){
  return _.some(config.users, { 'user': user, 'pass': pass });
};

app.use(boom());
app.use(cors({exposedHeaders:['content-range'],allowedHeaders:['range','Authorization','Content-Type']}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/swagger', express.static('public') );
app.use(function(req, res, next) {
    var user = auth(req);
    if (user && checkAuth(user['name'],user['pass'])){
        next();
    } else {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        res.boom.unauthorized('Authorization required').end();
    }
});

// Initialize epilogue
epilogue.initialize({
  app: app,
  sequelize: models.sequelize
});

// Projects REST resource
var projectResource = epilogue.resource({
  model: models.projects,
  endpoints: ['/projects', '/project/:id']
});

// Users REST resource
var usersResource = epilogue.resource({
    model: models.users,
    endpoints: ['/users', '/user/:id']
});

app.get('*', function(req, res) {
    res.boom.notImplemented().end();
});

// Create database and listen
models.sequelize
    .sync()
    .then(function() {
        app.listen(config.app.http_port, function() {
          console.log('listening at http://%s:%s', config.app.host, config.app.http_port);
        })
    })
    .catch(function(err){
        console.log(err);
    })
    .error(function(err){
        console.log(err);
    });

app.use(function (err, req, res, next) {
    if (!res.headersSent) {
        res.boom.badImplementation();
    }
});
