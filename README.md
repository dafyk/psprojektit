# Simple REST-API for MySQL
API exposes [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete "Create, read, update and delete") operations for MySQL tables.

## Environment variables
List of environment variables with default values. Defined in `config.js`:

#### HTTP settings
| name | default value |
| ------------ | ------------- |
| **APP_HTTP_HOST** | 127.0.0.1  |
| **PORT** | 8181  |

#### DATABASE settings
| name | default value |
| ------------ | ------------- |
| **DB_HOST** | 127.0.0.1 |
| **DB_PORT** | 3306 |
| **DB_DB** | rest_crud |
| **DB_USER** | root |
| **DB_PASS** | root |

#### AUTH settings
| name | default value |
| ------------ | ------------- |
| **AUTH_USER** | admin |
| **AUTH_PASS** | heslovica |

## Notes

In `package.json` is defined **postinstall** script which modifies **host** details in ***public/swagger.yaml*** file.
