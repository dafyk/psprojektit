"use strict";
var config = {};

config.app = {
      host: process.env.APP_HTTP_HOST || '127.0.0.1'
    , http_port: parseInt(process.env.PORT) || 8181
    , https_port: parseInt(process.env.APP_HTTPS_PORT) || 8182
    , salt: parseInt(process.env.APP_SALT) || 'vQylhJDgdq4FvoKNPY26Wx9e5DvqJ3eDmdHA6d1NpIJjGJjEJu'
};

config.db = {
    host: process.env.DB_HOST || '127.0.0.1'
  , port: parseInt(process.env.DB_PORT) || 3306
  , database : process.env.DB_DB || 'rest_crud'
  , user: process.env.DB_USER || 'root'
  , pass: process.env.DB_PASS || 'root'
  , options: {
        dialect: 'mysql'
      , host: process.env.DB_HOST || '127.0.0.1'
  }
};

config.users = [
    {'user': process.env.AUTH_USER || 'admin', 'pass': process.env.AUTH_PASS || 'heslovica'}
];
module.exports = config;
